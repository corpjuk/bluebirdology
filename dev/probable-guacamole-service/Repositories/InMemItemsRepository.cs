using System;
using System.Linq;
using System.Collections.Generic;
using Catalog.Models;

namespace Catalog.Repositories
{

    public class InMemItemsRepository
    {
        private readonly List<Item> items = new List<Item>()
        {
            new Item { Id = Guid.NewGuid(), FirstName = "Justin", LastName = "Cunningham", CreatedDate = DateTime.UtcNow},
            new Item { Id = Guid.NewGuid(), FirstName = "Matt", LastName = "Cunningham", CreatedDate = DateTime.UtcNow},
            new Item { Id = Guid.NewGuid(), FirstName = "Ryan", LastName = "Cunningham", CreatedDate = DateTime.UtcNow}
            
        };

        public IEnumerable<Item> GetItems()
        {
            return items;
        }

        public Item GetItem(Guid id)
        {
            return items.Where(item => item.Id == id).SingleOrDefault();
        }
    }
}