﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using probable_guacamole_service.Entities;

namespace probable_guacamole_service.Controllers
{
    [Controller]
    public abstract class BaseController : ControllerBase
    {
        // returns the current authenticated account (null if not logged in)
        public Account Account => (Account)HttpContext.Items["Account"];
    }
}
