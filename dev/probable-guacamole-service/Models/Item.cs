using System;

namespace Catalog.Models
    {
        //record instead of class - allows viewing not modifying
        public class Item
        {
            //init instead of set
            // can do
            // Item item = new() { Id = Guid.NewGuid() };
            // not
            // item.Id = Guid.NewGuid();
            public Guid Id { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public DateTimeOffset CreatedDate { get; set; }
        }
    }