﻿using System.ComponentModel.DataAnnotations;

namespace probable_guacamole_service.Models.Accounts
{
    public class VerifyEmailRequest
    {
        [Required]
        public string Token { get; set; }
    }
}