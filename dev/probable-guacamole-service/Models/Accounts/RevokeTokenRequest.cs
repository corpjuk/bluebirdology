﻿namespace probable_guacamole_service.Models.Accounts
{
    public class RevokeTokenRequest
    {
        public string Token { get; set; }
    }
}