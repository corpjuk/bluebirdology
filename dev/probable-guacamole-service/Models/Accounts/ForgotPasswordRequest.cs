﻿using System.ComponentModel.DataAnnotations;

namespace probable_guacamole_service.Models.Accounts
{
    public class ForgotPasswordRequest
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
    }
}