﻿//This is the sign in form
// https://www.youtube.com/watch?v=2m1lm8zuC7E

import React from "react";

import { makeStyles } from "@material-ui/core/styles";
import Typography from '@material-ui/core/Typography';
import FacebookIcon from '@material-ui/icons/Facebook';
import TwitterIcon from '@material-ui/icons/Twitter';
import LinkedInIcon from '@material-ui/icons/LinkedIn';

const useStyles = makeStyles({
    root: {
        padding: 0,
        margin: 0,
        fontFamily: "Roboto",
        fontWeight: 400,
        display: "flex",
        height: "100vh",
        justifyContent: "center",
        alignItems: "center",
        background: "linear-gradient(to bottom right, #27c6da, #5b6cc0)",
    },

    signupwrap: {
        width: "500px",
        boxshadow: "0 20px 25px -5px rgba(0,0,0, 0.1). 0 10px 10px -5px rgba(0, 0, 0, 0.04)",
        backgroundColor: "#fff",
    },

    signuptop: {
        '&:before': {
            content: "''",
            position: "absolute",
            height: "50px",
            bottom: "-20px",
            left: 0,
            right: 0,
            color: "red",
            transform: "skewY(-45deg)",
        },

        padding: "40px 40px 20px",
        position: "relative",
        display: "flex",
        flexDirection: "column",
    },



    signupbottom: {
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        backgroundColor: "#00b0f3",
        padding: "60px 40px 40px",
        color: "#fff",
    },

    inputwrap: {
        display: "flex",
        alignItems: "center",
        marginBottom: "32px",
    },

    //h2 tag is Sign up
    h2: {
        textTransform: "uppercase",
        fontWeight: 600,
        textAlign: "center",
        marginBottom: "32px",
        color: "#3a99d0",
    },

    h3: {
        fontWeight: 600,
        fontSize: "13px",
    },

    p: {
        fontSize: "13px",
        color: "#4e585f",
    },

    span: {
        color: "#3a99d0",
        fontWeight: 600,
        cursor: "pointer",
    },

    input: {
        '&:focus': {
            outline: "none",
            bowShadow: "0 1px 0 0 #88d9fc",
            borderColor: "#88d9fc",
        },
        fontSize: "14px",
        fontWeight: 500,
        padding: "4px 0",
        width: "100%",
        border: "none",
        borderBottom: "2px solid #e4f0f6",
        transition: "500ms ease all",
    },

    checkmark: {
        '&:focus': {
            outline: "none",
            boxShadow: "none",
            borderColor: "none",
        },
        height: "20px",
        width: "20px",
        marginRight: "16px",
    },

    button: {
        '&:hover': {
            backgroundColor: "#1f6b97",
            transform: "scale(0.98)",
        },
        cursor: "pointer",
        padding: "15px 60px",
        borderRadius: "30px",
        fontWeight: 600,
        border: "none",
        color: "#fff",
        backgroundColor: "#3a99d0",
        marginRight: "16px",
        transition: "500ms ease all",
    },

    ul: {
        '&:last-child': {
            marginRight: 0,
        },
        display: "flex",
        marginTop: "16px",
        listStyle: "none",
    },

    li: {
        marginRight: "40px",
        fontSize: "24px",
        cursor: "pointer",
    }

    
});

export default function Test() {
    const classes = useStyles();

    return (
        <>
            <div className={classes.root}>
                <div className={classes.signupwrap}>
                    <div className={classes.signuptop}>
                        <h2 className={classes.h2}>Sign up</h2>
                        <form>
                            <div className={classes.inputwrap}>
                                <input className={classes.input} type="text" placeholder="Email" />
                            </div>
                            <div className={classes.inputwrap}>
                                <input className={classes.input} type="text" placeholder="password" />
                            </div>
                            <div className={classes.inputwrap}>
                                <input type="checkbox" className={classes.checkmark} />
                                <span  />
                                <p className={classes.p}>Accept the <span className={classes.span}>Terms and Conditions</span></p>
                            </div>
                            <div className={classes.inputwrap}>
                                <button type="submit" className={classes.button}>Submit</button>

                                <p className={classes.p}>Already have an account? <span className={classes.span}>Log in</span></p>
                            </div>
                        </form>
                        <div className={classes.signupbottom}>
                            <h3 className={classes.h3}>Or sign up with: </h3>
                            <div className="link-container">
                                <ul className={classes.ul}>
                                    <li className={classes.li}>
                                        <FacebookIcon />
                                    </li>
                                    <li className={classes.li}>
                                        <TwitterIcon />
                                    </li>
                                    <li className={classes.li}>
                                        <LinkedInIcon />
                                    </li>
                                </ul>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </>
    );
}