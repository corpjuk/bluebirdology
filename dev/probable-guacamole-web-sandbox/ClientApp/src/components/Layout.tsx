import * as React from 'react';
import NavMenu from './NavMenu';
import Flexboxtest from './FlexBoxTest';

export default class Layout extends React.PureComponent<{}, { children?: React.ReactNode }> {
    public render() {
        return (
            <React.Fragment>
            <Flexboxtest />
            </React.Fragment>
        );
    }
}