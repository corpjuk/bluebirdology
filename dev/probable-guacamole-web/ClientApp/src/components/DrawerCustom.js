"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var core_1 = require("@material-ui/core");
var styles_1 = require("@material-ui/core/styles");
var react_router_dom_1 = require("react-router-dom");
var react_redux_1 = require("react-redux");
var Mail_1 = require("@material-ui/icons/Mail");
var Business_1 = require("@material-ui/icons/Business");
var Group_1 = require("@material-ui/icons/Group");
var AccountCircle_1 = require("@material-ui/icons/AccountCircle");
var Work_1 = require("@material-ui/icons/Work");
var Assignment_1 = require("@material-ui/icons/Assignment");
var SupervisedUserCircle_1 = require("@material-ui/icons/SupervisedUserCircle");
var AccountBalance_1 = require("@material-ui/icons/AccountBalance");
var Payment_1 = require("@material-ui/icons/Payment");
var Delete_1 = require("@material-ui/icons/Delete");
var DateRange_1 = require("@material-ui/icons/DateRange");
var PostAdd_1 = require("@material-ui/icons/PostAdd");
var Help_1 = require("@material-ui/icons/Help");
var Settings_1 = require("@material-ui/icons/Settings");
var styles = function () { return styles_1.createStyles({
    drawer: {
        width: "150px"
    }
}); };
var DrawerCustom = (function (_super) {
    __extends(DrawerCustom, _super);
    function DrawerCustom(props) {
        var _this = _super.call(this, props) || this;
        _this.classes = styles();
        _this.itemsList = [
            {
                text: "Home",
                icon: React.createElement(AccountCircle_1.default, null),
                onClick: function () {
                    _this.props.history.push("/");
                }
            },
            {
                text: "Profile",
                icon: React.createElement(AccountCircle_1.default, null),
                onClick: function () { return _this.props.history.push("/profile"); }
            },
            {
                text: "Candidates",
                icon: React.createElement(Group_1.default, null),
                onClick: function () { return _this.props.history.push("/candidates"); }
            },
            {
                text: "Facilities",
                icon: React.createElement(Business_1.default, null),
                onClick: function () { return _this.props.history.push("/facilities"); }
            },
            {
                text: "Current Employees",
                icon: React.createElement(Work_1.default, null),
            },
            {
                text: "Compliance",
                icon: React.createElement(Assignment_1.default, null),
            },
            {
                text: "Recruiting",
                icon: React.createElement(SupervisedUserCircle_1.default, null),
            },
            {
                text: "Accounting",
                icon: React.createElement(AccountBalance_1.default, null),
            },
            {
                text: "Payroll",
                icon: React.createElement(Payment_1.default, null),
            },
            {
                text: "Mail",
                icon: React.createElement(Mail_1.default, null),
            },
            {
                text: "Trash",
                icon: React.createElement(Delete_1.default, null),
            },
            {
                text: "Calender",
                icon: React.createElement(DateRange_1.default, null),
            },
            {
                text: "Activity Log",
                icon: React.createElement(Assignment_1.default, null),
            },
            {
                text: "Submit Candidate",
                icon: React.createElement(PostAdd_1.default, null),
            },
            {
                text: "Help",
                icon: React.createElement(Help_1.default, null),
            },
            {
                text: "Settings",
                icon: React.createElement(Settings_1.default, null),
            }
        ];
        return _this;
    }
    DrawerCustom.prototype.render = function () {
        return (React.createElement(core_1.Drawer, { variant: "permanent", className: this.classes.drawer },
            React.createElement(core_1.List, null, this.itemsList.map(function (item, index) {
                var text = item.text, icon = item.icon, onClick = item.onClick;
                return (React.createElement(core_1.ListItem, { button: true, key: text, onClick: onClick },
                    icon && React.createElement(core_1.ListItemIcon, null, icon),
                    React.createElement(core_1.ListItemText, { primary: text })));
            }))));
    };
    return DrawerCustom;
}(React.Component));
;
var mapStateToProps = function (state, ownProps) { return ({}); };
var mapDispatchToProps = function (dispatch) {
    return {
        changePage: function (page, push) {
            push(page);
        }
    };
};
exports.default = react_router_dom_1.withRouter(react_redux_1.connect(mapStateToProps, mapDispatchToProps)(DrawerCustom));
//# sourceMappingURL=DrawerCustom.js.map