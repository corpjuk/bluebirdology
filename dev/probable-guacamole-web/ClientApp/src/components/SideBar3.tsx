﻿import React from "react";
import { Drawer as MUIDrawer } from "@material-ui/core";
import { ListItem } from "@material-ui/core";
import { List } from "@material-ui/core";
import { ListItemIcon } from "@material-ui/core";
import { ListItemText } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

//icons

import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import GroupIcon from '@material-ui/icons/Group';
import BusinessIcon from '@material-ui/icons/Business';


const useStyles = makeStyles({
    drawer: {
        width: "200px"
    }
});

const Drawer = () => {
    const classes = useStyles();
    //create list as objects
    const itemList = [
        {
            text: "Profile",
            icon: <AccountCircleIcon />
        },
        {
            text: "Candidates",
            icon: <GroupIcon />
        },
        {
            text: "Facilities",
            icon: <BusinessIcon />
        },
    ];
    return (
        <MUIDrawer variant="temporary" className={classes.drawer}>
            <List>
                {itemList.map((item, index) => {
                    const { text } = item;
                    return (
                        <ListItem button key={text}>
                            <ListItemText primary={text} />
                        </ListItem>
                    );
                })}
         </List>
        </MUIDrawer>
    );
};

export default Drawer;