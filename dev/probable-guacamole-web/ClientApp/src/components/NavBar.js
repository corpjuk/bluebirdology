"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var styles_1 = require("@material-ui/core/styles");
var AppBar_1 = require("@material-ui/core/AppBar");
var Toolbar_1 = require("@material-ui/core/Toolbar");
var Typography_1 = require("@material-ui/core/Typography");
var Button_1 = require("@material-ui/core/Button");
var useStyles = styles_1.makeStyles(function (theme) { return ({
    root: {
        flexGrow: 1,
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    title: {
        flexGrow: 1,
    },
    header: {
        backgroundColor: "dark blue",
        color: "white",
    }
}); });
function ButtonAppBar() {
    var classes = useStyles();
    return (React.createElement("div", { className: classes.root },
        React.createElement(AppBar_1.default, { position: "static", className: classes.header },
            React.createElement(Toolbar_1.default, null,
                React.createElement(Typography_1.default, { variant: "h6", className: classes.title }),
                React.createElement(Button_1.default, { color: "inherit" }, "Login")))));
}
exports.default = ButtonAppBar;
//# sourceMappingURL=NavBar.js.map