﻿import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';

const useStyles = makeStyles((theme) => ({
    root: {
        marginLeft: '250px',
        flexGrow: 1,
    },
    paper: {
        padding: theme.spacing(2),
        textAlign: 'left',
        color: theme.palette.text.secondary,
    },
}));

const Profile = () => {
    const classes = useStyles();
    return <div className={classes.root}>
        <Grid container spacing = {3}>
        <Grid item xs={6}>
                <Paper className={classes.paper}>
                    <h1>Personal Information</h1>
                    <h3>Name:</h3>
                    <h3>Email:</h3>
                    <h3>Phone Number:</h3>
                    <h3>Date of Birth:</h3>
                    <h3>Emergency Contact:</h3>
                    <h3>Emergency Contact Phone:</h3>
                    <h3>Tax Home Address:</h3>

                </Paper>
        </Grid>
        <Grid item xs={6}>
                <Paper className={classes.paper}>
                <h3>Notes:</h3>
                </Paper>
            </Grid>
        </Grid>
        <Grid item xs={6}>
            <Paper className={classes.paper}>
                <h1>Lisences:</h1>

            </Paper>
        </Grid>
        <Grid item xs={6}>
            <Paper className={classes.paper}>
                <h1>Certificates</h1>

            </Paper>
        </Grid>
        <Grid item xs={6}>
            <Paper className={classes.paper}>
                <h1>Education:</h1>

            </Paper>
        </Grid>
        <Grid item xs={6}>
            <Paper className={classes.paper}>
                <h1>Work History:</h1>

            </Paper>
        </Grid>
        <Grid item xs={6}>
            <Paper className={classes.paper}>
                <h1>Skills:</h1>

            </Paper>
        </Grid>
        <Grid item xs={6}>
            <Paper className={classes.paper}>
                <h1>References:</h1>

            </Paper>
        </Grid>
        <Grid item xs={6}>
            <Paper className={classes.paper}>
                <h1>Medical Documents:</h1>

            </Paper>
        </Grid>
        <Grid item xs={6}>
            <Paper className={classes.paper}>
                <h1>Additional Documents:</h1>

            </Paper>
        </Grid>
        <Grid item xs={6}>
            <Paper className={classes.paper}>
                <h1>Work Authorization:</h1>

            </Paper>
        </Grid>



    </div>
};

export default Profile;