﻿import * as React from "react";
import { Drawer as MUIDrawer, ListItem, List, ListItemIcon, ListItemText} from "@material-ui/core";
import { createStyles, withStyles, WithStyles } from "@material-ui/core/styles";
import { RouteComponentProps, withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { push } from 'connected-react-router'

import InboxIcon from "@material-ui/icons/MoveToInbox";
import MailIcon from "@material-ui/icons/Mail";
import BusinessIcon from '@material-ui/icons/Business';
import GroupIcon from '@material-ui/icons/Group';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import PermContactCalendarIcon from '@material-ui/icons/PermContactCalendar';
import WorkIcon from '@material-ui/icons/Work';
import AssignmentIcon from '@material-ui/icons/Assignment';
import SupervisedUserCircleIcon from '@material-ui/icons/SupervisedUserCircle';
import AccountBalanceIcon from '@material-ui/icons/AccountBalance';
import PaymentIcon from '@material-ui/icons/Payment';
import DeleteIcon from '@material-ui/icons/Delete';
import DateRangeIcon from '@material-ui/icons/DateRange';
import SubjectIcon from '@material-ui/icons/Subject';
import PostAddIcon from '@material-ui/icons/PostAdd';
import HelpIcon from '@material-ui/icons/Help';
import SettingsIcon from '@material-ui/icons/Settings';
import { StaticContext } from "react-router";

const styles = () => createStyles({
    drawer: {
        width: "150px"
    }
})

// These props are provided when creating the component
interface OwnProps {
    history: any,
    location: any,
    match: any,
    staticContext: any
}

// These props are provided via connecting the component store
interface StateProps {
    history: string[]
}

class DrawerCustom extends React.Component<OwnProps, StateProps> {

    constructor(props: any) {
        super(props);
    }

    classes: any = styles();
    itemsList: any = [
        {
            text: "Home",
            icon: <AccountCircleIcon />,
            onClick: () => {
                this.props.history.push("/");
                //console.log(this.props);
            }
        },
        {
            text: "Profile",
            icon: <AccountCircleIcon />,
            onClick: () => this.props.history.push("/profile")
        },
        {
            text: "Candidates",
            icon: <GroupIcon />,
            onClick: () => this.props.history.push("/candidates")
        },
        {
            text: "Facilities",
            icon: <BusinessIcon />,
            onClick: () => this.props.history.push("/facilities")
        },
        {
            text: "Current Employees",
            icon: <WorkIcon />,
            //onClick: () => this.state.history.push("/")
        },
        {
            text: "Compliance",
            icon: <AssignmentIcon />,
            //onClick: () => this.state.history.push("/about")
        },
        {
            text: "Recruiting",
            icon: <SupervisedUserCircleIcon />,
            //onClick: () => this.state.history.push("/contact")
        },
        {
            text: "Accounting",
            icon: <AccountBalanceIcon />,
            //onClick: () => this.state.history.push("/")
        },
        {
            text: "Payroll",
            icon: <PaymentIcon />,
            //onClick: () => this.state.history.push("/about")
        },
        {
            text: "Mail",
            icon: <MailIcon />,
            //onClick: () => this.state.history.push("/contact")
        },
                {
            text: "Trash",
            icon: <DeleteIcon />,
            //onClick: () => this.state.history.push("/facilities")
        },
        {
            text: "Calender",
            icon: <DateRangeIcon />,
            //onClick: () => this.state.history.push("/")
        },
        {
            text: "Activity Log",
            icon: <AssignmentIcon />,
            //onClick: () => this.state.history.push("/about")
        },
        {
            text: "Submit Candidate",
            icon: <PostAddIcon />,
            //onClick: () => this.state.history.push("/contact")
        },
        {
            text: "Help",
            icon: <HelpIcon />,
            //onClick: () => this.state.history.push("/")
        },
        {
            text: "Settings",
            icon: <SettingsIcon />,
            //onClick: () => this.state.history.push("/")
        }
    ];

    render() {
        return (
            <MUIDrawer variant="permanent" className={this.classes.drawer}>
                <List>
                    {this.itemsList.map((item: { text: string; icon: any; onClick: any; }, index: any) => {
                        const { text, icon, onClick } = item;
                        return (
                            <ListItem button key={text} onClick={onClick}>
                                {icon && <ListItemIcon>{icon}</ListItemIcon>}
                                <ListItemText primary={text} />
                            </ListItem>
                        );
                    })}
                </List>
            </MUIDrawer>
        );
    }
};

const mapStateToProps = (state: any, ownProps: any) => ({
});

const mapDispatchToProps = (dispatch: any) => {
    return {
        changePage: (page: string, push: any) => {
            push(page);
        }
    }
}

export default withRouter(
    connect(mapStateToProps, mapDispatchToProps)(DrawerCustom)
);