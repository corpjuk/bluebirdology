"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var NavBar_1 = require("./NavBar");
var DrawerCustom_1 = require("./DrawerCustom");
exports.default = (function (props) { return (React.createElement(React.Fragment, null,
    React.createElement(NavBar_1.default, null),
    React.createElement(DrawerCustom_1.default, null),
    props.children)); });
//# sourceMappingURL=Layout.js.map