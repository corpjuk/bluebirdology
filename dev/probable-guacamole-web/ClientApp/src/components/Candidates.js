"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var styles_1 = require("@material-ui/core/styles");
var Table_1 = require("@material-ui/core/Table");
var TableBody_1 = require("@material-ui/core/TableBody");
var TableCell_1 = require("@material-ui/core/TableCell");
var TableContainer_1 = require("@material-ui/core/TableContainer");
var TableHead_1 = require("@material-ui/core/TableHead");
var TableRow_1 = require("@material-ui/core/TableRow");
var Paper_1 = require("@material-ui/core/Paper");
var useStyles = styles_1.makeStyles({
    root: {
        marginLeft: '250px'
    },
    table: {
        minWidth: 650,
    },
});
function createData(name, calories, fat, carbs, protein) {
    return { name: name, calories: calories, fat: fat, carbs: carbs, protein: protein };
}
var rows = [
    createData('Frozen yoghurt', 159, 6.0, 24, 4.0),
    createData('Ice cream sandwich', 237, 9.0, 37, 4.3),
    createData('Eclair', 262, 16.0, 24, 6.0),
    createData('Cupcake', 305, 3.7, 67, 4.3),
    createData('Gingerbread', 356, 16.0, 49, 3.9),
];
var Candidates = function () {
    var classes = useStyles();
    return (React.createElement("div", { className: classes.root },
        React.createElement(TableContainer_1.default, { component: Paper_1.default },
            React.createElement(Table_1.default, { className: classes.table, "aria-label": "simple table" },
                React.createElement(TableHead_1.default, null,
                    React.createElement(TableRow_1.default, null,
                        React.createElement(TableCell_1.default, null, "Dessert (100g serving)"),
                        React.createElement(TableCell_1.default, { align: "right" }, "Calories"),
                        React.createElement(TableCell_1.default, { align: "right" }, "Fat\u00A0(g)"),
                        React.createElement(TableCell_1.default, { align: "right" }, "Carbs\u00A0(g)"),
                        React.createElement(TableCell_1.default, { align: "right" }, "Protein\u00A0(g)"))),
                React.createElement(TableBody_1.default, null, rows.map(function (row) { return (React.createElement(TableRow_1.default, { key: row.name },
                    React.createElement(TableCell_1.default, { component: "th", scope: "row" }, row.name),
                    React.createElement(TableCell_1.default, { align: "right" }, row.calories),
                    React.createElement(TableCell_1.default, { align: "right" }, row.fat),
                    React.createElement(TableCell_1.default, { align: "right" }, row.carbs),
                    React.createElement(TableCell_1.default, { align: "right" }, row.protein))); }))))));
};
exports.default = Candidates;
//# sourceMappingURL=Candidates.js.map