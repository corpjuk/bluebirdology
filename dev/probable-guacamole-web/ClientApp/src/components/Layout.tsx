import * as React from 'react';
import NavBar from './NavBar';
import DrawerCustom from './DrawerCustom';


export default (props: { children?: React.ReactNode }) => (
    <React.Fragment>
        <NavBar />
        <DrawerCustom />
        {props.children}
    </React.Fragment>
);
