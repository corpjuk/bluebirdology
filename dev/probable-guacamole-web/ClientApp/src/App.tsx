import * as React from 'react';
import { Route } from 'react-router';
import Layout from './components/Layout';
import Home from './components/Home';
import Counter from './components/Counter';
import FetchData from './components/FetchData';
import Profile from './components/Profile';
import Candidates from './components/Candidates';
import Facilities from './components/Facilities';

import './custom.css';

export default () => (
    <Layout>
        <Route exact path='/' component={Home} />
        <Route path='/counter' component={Counter} />
        <Route path='/profile' component={Profile} />
        <Route path='/candidates' component={Candidates} />
        <Route path='/facilities' component={Facilities} />
        <Route path='/fetch-data/:startDateIndex?' component={FetchData} />
    </Layout>
);
