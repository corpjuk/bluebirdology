"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var react_router_1 = require("react-router");
var Layout_1 = require("./components/Layout");
var Home_1 = require("./components/Home");
var Counter_1 = require("./components/Counter");
var FetchData_1 = require("./components/FetchData");
var Profile_1 = require("./components/Profile");
var Candidates_1 = require("./components/Candidates");
var Facilities_1 = require("./components/Facilities");
require("./custom.css");
exports.default = (function () { return (React.createElement(Layout_1.default, null,
    React.createElement(react_router_1.Route, { exact: true, path: '/', component: Home_1.default }),
    React.createElement(react_router_1.Route, { path: '/counter', component: Counter_1.default }),
    React.createElement(react_router_1.Route, { path: '/profile', component: Profile_1.default }),
    React.createElement(react_router_1.Route, { path: '/candidates', component: Candidates_1.default }),
    React.createElement(react_router_1.Route, { path: '/facilities', component: Facilities_1.default }),
    React.createElement(react_router_1.Route, { path: '/fetch-data/:startDateIndex?', component: FetchData_1.default }))); });
//# sourceMappingURL=App.js.map